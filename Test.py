import unittest
from CaseCorrector import getcorrectcase, compute_casematch_accuracy


class Test(unittest.TestCase):

    '''
    A unit test case that passes if the accuracy comes out to be greater than 95 percent.
    '''
    def test_getcorrectcase(self):
        inputString = "NHS SERVICES ACROSS ENGLAND AND SOME IN SCOTLAND HAVE BEEN HIT BY IT FAILURE, CAUSED BY A LARGE-SCALE CYBER-ATTACK."
        targetString = "NHS services across England and some in Scotland have been hit by IT failure, caused by a large-scale cyber-attack."
        accuracy = compute_casematch_accuracy(targetString,getcorrectcase(inputString))
        assert accuracy >=0.95 , 'Accuracy is between 95 and 100 percent'


if __name__ == '__main__':
    unittest.main()