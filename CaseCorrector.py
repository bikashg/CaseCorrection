# External resources needed :
# A) Java 8 must be installed in the system (for nltk calling the stanford corenlp)


import sys
from nltk.tag import StanfordNERTagger

stanford_ner_dir = 'lib/stanford-ner-2016-10-31/'
eng_model_filename= stanford_ner_dir + 'classifiers/english.conll.4class.distsim.crf.ser.gz'
my_path_to_jar= stanford_ner_dir + 'stanford-ner.jar'

st = StanfordNERTagger(model_filename=eng_model_filename, path_to_jar=my_path_to_jar)



'''
Input : A string with/without correct case. 
Return : The same string but with correct case.
'''
def getcorrectcase(line) :
    output = ""
    ner_tagged = st.tag(line.split())  # This is a list of tuples -- each tuple being word,tag pair
    for (word, tag) in ner_tagged:
        if tag == "O":
            output = output + " " + word.lower()
        else:
            output = output + " " + word.capitalize()  # Makes the first character in this word capital
    return output.strip()


'''
Input : Filename whose contents are to be read
Return : A list containing each line of text in that file
'''
def readfile(inputfilename) :
    lines = []
    with open(inputfilename) as f:
        for line in f:
            lines.append(line.strip())
    return lines


'''
Input : Takes two strings
Output : Ratio of match of characters (case-sensitive) in those strings
'''
def compute_casematch_accuracy(string1,string2) :
    if len(string1)!=len(string2) :
        return 0
    countaccuracy = 0
    for i in range(len(string1)) :
        char1 = string1[i]
        char2 = string2[i]
        if char1 == char2 :
            countaccuracy += 1
    return countaccuracy/len(string1)









if __name__ == '__main__':

    if (len(sys.argv)<2) :
        print ("\nPlease supply the name of the input file. Output will be written to console. See example command in runAll.sh file")
        exit(1)

    if (len(sys.argv)>3) :
        print ("\nMin 1 argument : Input file and Max 2 arguments: Input file and Reference file. Output will be written to console. See example command in runAll.sh file")
        exit(1)

    input_lines = readfile(sys.argv[1])
    results = []
    for i in range(len(input_lines)):
        result = getcorrectcase(input_lines[i])
        # An extra step -- The first letter in any sentence must be in capital case.
        tmp = result[0].upper()
        for j in range(1, len(result)):
            tmp = tmp + result[j]
        result = tmp
        results.append(result)


    print("\n")


    if (len(sys.argv)==2) :
        for i in range(len(input_lines)):
            print(results[i])


    if (len(sys.argv)==3) :
        target_lines = readfile(sys.argv[2])
        for i in range(len(input_lines)):
            target = target_lines[i]
            print("\n\nInput = " + input_lines[i] + "\n" + "Output = " + results[i] + "\nTarget = " + target + "\nAccuracy = " + str(compute_casematch_accuracy(results[i], target)))
