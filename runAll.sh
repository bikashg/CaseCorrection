#!/usr/bin/env bash
# To run with just the input file
python3 CaseCorrector.py data/Input.txt

# To run with both the input file and the target file
python3 CaseCorrector.py data/Input.txt data/Target.txt