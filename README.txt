Code Explanation :

In English, we use capital letter at the beginning of any sentence.
Additionally, we also capitalize the first letter of words representing named entities;
eg. places, persons and organizations. The rest of the sentence usually consists
of small letter words.

Based on these observations, I first used the Stanford Named Entity recognizer library
to identify words representing named entities in my input sentences. For each such word, I
rendered all its letters to small case except for the first one. For all other words in the
sentence, I put them in small case. Finally, for the first letter beginning the sentence, I
converted it to capital case.



Shortcomings and Improvements possible :

My approach fails with abbreviations. Even when they are correctly identified as named entities
by the library (eg: NHS), my approach of keeping only the first letter of named entities means that
such abbreviations are treated as normal words. In other instances, the library has not been trained
to identify all named entities (eg: Mercedes, IPL). The latter case also signifies the evolving nature
of human languages -- no library can be built to keep up with names coming up in future.

One way to overcome these limitations would be to take advantage of task-specific domain vocabularies to
identify named entities (including abbreviations). Such vocabularies could, for example, be available as
open source resource on the web.





How to Run

The 3 scripts can be executed independently as :
                i) ./run1.sh   or   ii) ./run2.sh      or   iii) ./runAll.sh
run1.sh -- takes the file at data/Input.txt as input. Displays the result in the console.
run2.sh -- takes the file at data/Input.txt as input and the file at data/target.txt as reference output.
           Processes the input file, obtains result and computes the accuracy of this result with the reference output.
           Displays the result and the accuracy percentage at the console.
runAll.sh -- carries out the operations described in i) and ii)

To take any other file (say /home/user1/testfile.txt) as input, simply run the following command :
python3 CaseCorrector.py /home/user1/testfile.txt



Test :

I wrote a simple test comparing my program output to the reference output desired. The idea here was pretty simple -- count the number of times my program outputs a letter with the
 same case as in the reference output. If this count was above a certain threshold, I considered it as a pass.
